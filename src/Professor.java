
public class Professor extends Person{
	private String title;
	public Professor(String fName, String lName, String intro, String t) {
		super(fName, lName, intro);
		this.title = t;
	}
}
