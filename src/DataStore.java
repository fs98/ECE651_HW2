import java.util.ArrayList;

public class DataStore {
	private ArrayList<Person> records = new ArrayList<>();
	
	public ArrayList<Person> getRecords() {
		return records;
	}
	public void addRecord(Person p) {
		records.add(p);
	}
	public void deleteRecord(Person p) {
		records.remove(p);
	}
	public void printAll() {
		for(Person p : this.getRecords()) {
			System.out.println(p.getfName() + p.getlName() + ":");
			System.out.println(p.whoIs(this, p.getfName(), p.getlName()) + "\n");
		}
	}
}
