
public class test {
	public static void main(String[] args) {
		DataStore d = new DataStore();
		
		String s1 = "Fangyuan Sun is from China and is a MS ECE student";
		
		String s2 = "Shalin Shah is from India and is a PhD ECE candidate. "
				+ "He does not have prior work experience. "
				+ "He received his undergraduate from DA-IICT. "
				+ "When not in class, Shalin enjoys bodybuilding and dancing.";
		
		String s3 = "Anil Ganti is from NJ and is a PhD ECE candidate. "
				+ "He worked for a few software start-ups. "
				+ "and he enjoys climbing";
		
		String s4 = "Niral Shal is from NJ and is a MEng ECE student. "
				+ "He went to undergrad at Rutgers University. "
				+ "and he enjoy playing tennis and reading the news.";
		
		String s5 = "Ric Telford is an adjunct Associate Professor at duke. "
				+ "and he is also a founder of Telford Ventures LLC.";
		
		String t1 = "Executive-in-Residence and Adjunct Assoc. Professor";
				
		
		BlueDevil p1 = new BlueDevil("Fangyuan" , "Sun" , s1 , "ECE");
		BlueDevil p2 = new BlueDevil("Shalin" , "Shah" , s2 , "ECE");
		BlueDevil p3 = new BlueDevil("Anil" , "Ganti" , s3 , "ECE");
		BlueDevil p4 = new BlueDevil("Niral" , "Shal" , s4 , "ECE");
		Professor p5 = new Professor("Ric" , "Telford" , s5 , t1);
	
		d.addRecord(p1);
		d.addRecord(p2);
		d.addRecord(p3);
		d.addRecord(p4);
		d.addRecord(p5);
		
		System.out.println("Show all person in the database:\n");
		d.printAll();
		
		d.deleteRecord(p1);
		d.deleteRecord(p2);
		d.deleteRecord(p3);
		
		System.out.println("After delete, Show all person in the database:\n");
		d.printAll();
	}
}
