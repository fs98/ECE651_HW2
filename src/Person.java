import java.util.ArrayList;

public class Person {
	private String firstName;
	private String lastName;
	private String introduction;
	
	public Person(String fName, String lName, String intro) {
		this.firstName = fName;
		this.lastName = lName;
		this.introduction = intro;
	}
	
	public String getfName() {
		return firstName;
	}
	
	public String getlName() {
		return lastName;
	}
	
	public String whoIs(DataStore records , String fName, String lName) {
		ArrayList<Person> data = records.getRecords();
		for(Person p : data) {
			if(p.firstName == fName && p.lastName == lName) {
				return p.introduction;
			}
		}
		return " ";	
	}
}
